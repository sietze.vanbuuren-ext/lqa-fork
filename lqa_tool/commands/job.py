###################################################################################
# LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from __future__ import print_function

from lqa_api.job import Job, JobError
from lqa_api.exit_codes import APPLICATION_ERROR
from lqa_tool.commands import Command
from lqa_tool.settings import lqa_logger

class JobCmd(Command):

    def __init__(self, args):
        Command.__init__(self, args)

    def run(self):
        for job_id in self.job_ids:
            job = Job(job_id, self.server)
            try:
                print(job)
                # Show further job metadata with --info
                if self.args.info:
                    print("  status: {}".format(job.status))
                    print("  bundle: {}".format(job.bundle_link))
                    if job.sha1:
                        print("  sha1: {}".format(job.sha1))
                    print("  submit time: {}".format(job.submit_time))
                    if job.start_time:
                        print("  start time: {}".format(job.start_time))
                    if job.end_time:
                        print("  end time: {}".format(job.end_time))
                        # If there is an end_time, there _should_ be a start_time
                        # so it is good to calculate duration here.
                        print("  duration: {}" \
                            .format(job.end_time - job.start_time))
                    print("  priority: {}".format(job.priority))
                    print("  device type: {}".format(job.device_type))
                    print("  hostname: {}".format(job.hostname))
                    print("  worker: {}".format(job.worker_host))
                # Skip tests if --no-tests passed
                if self.args.tests:
                    for test in job.tests:
                        print("|", test)
            except JobError as err:
                lqa_logger.error("job command: job {}: {} {}" \
                                     .format(job_id, err.code, err.msg))
                exit(APPLICATION_ERROR)
